package com.cleartrip.tripsuggester.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TripSuggesterUtil {

    public static String makePostRequest(String url, String requestBody){
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost postMethod = new HttpPost(url);
        StringEntity requestEntity = new StringEntity(requestBody,
                ContentType.APPLICATION_JSON);
        postMethod.setEntity(requestEntity);
        try {
            HttpResponse response = httpClient.execute(postMethod);
            int responseCode = response.getStatusLine().getStatusCode();
            if(responseCode == 200){
                InputStream inputStream = response.getEntity().getContent();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String responseString = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    responseString += line;
                }
                return responseString;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String readFromFile(String filePath){
        try {
            String content = new String(Files.readAllBytes(Paths.get(filePath)));
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
