package com.cleartrip.tripsuggester.controller;

import com.cleartrip.tripsuggester.constants.TripType;
import com.cleartrip.tripsuggester.factory.ProductFactoryManager;
import com.cleartrip.tripsuggester.service.TripSuggestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TripSuggestController {

    TripSuggestService tripSuggestService;

    @Autowired
    ProductFactoryManager productFactoryManager;

    @RequestMapping("curated_hotels")
    public String getCuratedHotels(){
        TripType tripType = TripType.CURATED_HOTELS;
        String result = getResultByTripType(tripType);
        return result;
    }

    @RequestMapping("top_rated_hotels")
    public String getTopRatedHotels(){
        TripType tripType = TripType.TOP_RATED;
        String result = getResultByTripType(tripType);
        return result;    }

    @RequestMapping("popular_destinations")
    public String getPopularDestinations(){
        TripType tripType = TripType.POPULAR_DESTINATIONS;
        String result = getResultByTripType(tripType);
        return result;
    }

    @RequestMapping("explore_city")
    public String getPlacesAroundCity(){
        TripType tripType = TripType.CITY_EXPLORE;
        String result = getResultByTripType(tripType);
        return result;
    }

    private String getResultByTripType(TripType tripType){
        tripSuggestService = productFactoryManager.getTripSuggestService(tripType);
        String result = tripSuggestService.getSuggestedTripsFromCache(tripType.name());
        if(result == null || result.equals("")){
            result = tripSuggestService.getTripDetailsJson();
            if(result != null && !result.equals("")){
                tripSuggestService.addResultsToCache(tripType.name(), result, 100);
            }
        }
        return result;

    }


}
