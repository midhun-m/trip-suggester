package com.cleartrip.tripsuggester.factory;

import com.cleartrip.tripsuggester.constants.TripType;
import com.cleartrip.tripsuggester.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductFactoryManager {

    @Autowired
    private CityExploreServiceImpl cityExploreService;

    @Autowired
    private CuratedHotelServiceImpl curatedHotelService;

    @Autowired
    private PopularDestinationServicImpl popularDestinationService;

    @Autowired
    private TopRatedHotelServiceImpl topRatedHotelService;

    public TripSuggestService getTripSuggestService(TripType tripType){

        switch(tripType){
            case CITY_EXPLORE:
                return cityExploreService;
            case CURATED_HOTELS:
                return curatedHotelService;
            case POPULAR_DESTINATIONS:
                return popularDestinationService;
            case TOP_RATED:
                return topRatedHotelService;
        }
        return null;
    }



}
