package com.cleartrip.tripsuggester;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripSuggesterApplication {

	public static void main(String[] args) {
		SpringApplication.run(TripSuggesterApplication.class, args);
	}
}
