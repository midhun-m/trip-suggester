package com.cleartrip.tripsuggester.data;

import com.google.gson.annotations.SerializedName;

public class Hotel {

    @SerializedName("hotel_id")
    int hotelId;
    @SerializedName("hotel_name")
    String hotelName;
    @SerializedName("location")
    String location;
    @SerializedName("ta_rating")
    Double taRating;
    @SerializedName("star_rating")
    Double starRating;
    @SerializedName("price")
    int price;
    @SerializedName("image_url")
    String imageUrl;
    @SerializedName("page_url")
    String pageUrl;

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getTaRating() {
        return taRating;
    }

    public void setTaRating(Double taRating) {
        this.taRating = taRating;
    }

    public Double getStarRating() {
        return starRating;
    }

    public void setStarRating(Double starRating) {
        this.starRating = starRating;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }
}
