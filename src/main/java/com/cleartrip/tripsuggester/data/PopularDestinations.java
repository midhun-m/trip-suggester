package com.cleartrip.tripsuggester.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PopularDestinations {

    @SerializedName("message")
    String message;

    @SerializedName("destinations")
    List<Destination> destinations;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }
}
