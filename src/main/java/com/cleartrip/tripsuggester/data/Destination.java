package com.cleartrip.tripsuggester.data;

import com.google.gson.annotations.SerializedName;

public class Destination {

    @SerializedName("name")
    String name;

    @SerializedName("page_url")
    String pageUrl;

    @SerializedName("image_url")
    String imageUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
