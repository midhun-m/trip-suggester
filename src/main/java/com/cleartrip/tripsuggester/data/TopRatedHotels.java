package com.cleartrip.tripsuggester.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopRatedHotels {

    @SerializedName("msg")
    String message;

    @SerializedName("hotels")
    List<Hotel> hotels;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }
}
