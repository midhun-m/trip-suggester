package com.cleartrip.tripsuggester.data;

import java.util.List;

public class ThingsToDo {

    String message;
    List<ActivityDestination> activityDestinations;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ActivityDestination> getActivityDestinations() {
        return activityDestinations;
    }

    public void setActivityDestinations(List<ActivityDestination> activityDestinations) {
        this.activityDestinations = activityDestinations;
    }
}
