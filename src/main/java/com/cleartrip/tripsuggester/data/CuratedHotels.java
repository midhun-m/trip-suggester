package com.cleartrip.tripsuggester.data;

import java.util.List;

public class CuratedHotels {

    String message;
    List<CuratedHotelData> hotelList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CuratedHotelData> getHotelList() {
        return hotelList;
    }

    public void setHotelList(List<CuratedHotelData> hotelList) {
        this.hotelList = hotelList;
    }
}
