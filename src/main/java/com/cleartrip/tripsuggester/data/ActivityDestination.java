package com.cleartrip.tripsuggester.data;

public class ActivityDestination {

    String anchorTag;
    String url;

    public String getAnchorTag() {
        return anchorTag;
    }

    public void setAnchorTag(String anchorTag) {
        this.anchorTag = anchorTag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
