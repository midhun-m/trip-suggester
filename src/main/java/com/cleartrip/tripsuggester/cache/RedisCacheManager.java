package com.cleartrip.tripsuggester.cache;

import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;


@Repository
public class RedisCacheManager implements CacheManager{

    JedisPool jedisPool;
    Jedis jedis;

    public RedisCacheManager(JedisPool jedisPool, Jedis jedis){
        this.jedisPool = jedisPool;
        this.jedis = jedis;
    }

    public void addString(String key, String value, int timeInSeconds){
        jedis.set(key,value);
        jedis.expire(key, timeInSeconds);
    }

    public String getString(String key){
        String str = jedis.get(key);
        return str;
    }

}
