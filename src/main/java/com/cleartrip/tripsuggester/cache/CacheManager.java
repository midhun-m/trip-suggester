package com.cleartrip.tripsuggester.cache;

public interface CacheManager {

    public void addString(String key, String value, int timeInSeconds);

    public String getString(String key);

}
