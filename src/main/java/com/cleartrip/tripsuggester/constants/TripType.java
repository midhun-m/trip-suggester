package com.cleartrip.tripsuggester.constants;

public enum TripType {
    CURATED_HOTELS("curated_hotels"),
    CITY_EXPLORE("city_explore"),
    POPULAR_DESTINATIONS("popular_destinations"),
    TOP_RATED("top_rated");

    private final String trip;
    TripType(String trip){
        this.trip = trip;
    }

    public String getTripType(){
        return this.trip;
    }

}
