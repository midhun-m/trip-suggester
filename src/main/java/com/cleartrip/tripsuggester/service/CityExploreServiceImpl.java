package com.cleartrip.tripsuggester.service;

import com.cleartrip.tripsuggester.data.ActivityDestination;
import com.cleartrip.tripsuggester.data.ThingsToDo;
import com.cleartrip.tripsuggester.util.TripSuggesterUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class CityExploreServiceImpl extends BaseTripSuggestService{

    @Autowired
    Environment env;

    private String getBlockMessage(){
        String message = env.getProperty("com.cleartrip.things.to.do.block.message");
        return message;
    }

    private Map<String, Map<String, String>> getActivitiesData(){
        try{
            String thingsToDoFile = env.getProperty("com.cleartrip.things.to.do.data.file");
            String destinationDataJson = TripSuggesterUtil.readFromFile(thingsToDoFile);
            Gson gson = new Gson();
            Type type = new TypeToken<Map<String, Map<String, String>>>(){}.getType();
            Map<String, Map<String, String>> thingsToDoData = gson.fromJson(destinationDataJson, type);
            return thingsToDoData;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private List<String> getActivitiesToSuggest(){
        String thingsToDo = env.getProperty("com.cleartrip.things.to.do.list");
        List<String> thingsToDoList = Arrays.asList(thingsToDo);
        return thingsToDoList;
    }

    private ActivityDestination createActivityDestination( Map<String, String> activityData){

        ActivityDestination activityDestination = new ActivityDestination();
        if(activityData.containsKey("anchor_tag"))
            activityDestination.setAnchorTag(activityData.get("anchor_tag"));
        if(activityData.containsKey("link"))
            activityDestination.setUrl(activityData.get("link"));
        String url = activityData.get("link");
        activityDestination.setUrl(url);
        return activityDestination;
    }

    @Override
    public String getTripDetailsJson() {

        ThingsToDo thingsToDo = new ThingsToDo();

        String message = getBlockMessage();
        thingsToDo.setMessage(message);

        List<String> activitiesToSuggest = getActivitiesToSuggest();

        Map<String, Map<String, String>> activitiesData = getActivitiesData();
        if(activitiesData != null){
            List<ActivityDestination> activityDestinations = new ArrayList<>();
            for(String activityToSuggest: activitiesToSuggest){
                if(activitiesData.containsKey(activityToSuggest)){
                    Map<String, String> activityData = activitiesData.get(activityToSuggest);
                    ActivityDestination activityDestination = createActivityDestination(activityData);
                    activityDestinations.add(activityDestination);
                }
            }
            thingsToDo.setActivityDestinations(activityDestinations);
        }
        Gson gson = new Gson();
        return gson.toJson(thingsToDo);
    }
}
