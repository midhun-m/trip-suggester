package com.cleartrip.tripsuggester.service;


import com.cleartrip.tripsuggester.data.Destination;
import com.cleartrip.tripsuggester.data.PopularDestinations;
import com.cleartrip.tripsuggester.util.TripSuggesterUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.*;

@Component
public class PopularDestinationServicImpl extends BaseTripSuggestService{

    @Autowired
    Environment env;

    public Set<String> getTripsToSuggest() {
        String popularDestinations = env.getProperty("com.cleartrip.popular.destination.list");
        List<String> popularDestinationList = Arrays.asList(popularDestinations.split(","));

        return new HashSet<>(popularDestinationList);
    }

    private PopularDestinations getPopularDestinationJson(){
        Gson gson = new Gson();
        String dataFilePath = env.getProperty("com.cleartrip.popular.destination.data.file");
        String destinationDataJson = TripSuggesterUtil.readFromFile(dataFilePath);
        PopularDestinations popularDestinations = gson.fromJson(destinationDataJson, PopularDestinations.class);

        return popularDestinations;
    }

    @Override
    public String getTripDetailsJson() {
        Set<String> destinationSet = getTripsToSuggest();

        PopularDestinations popularDestinationJson = getPopularDestinationJson();
        List<Destination> destinations = popularDestinationJson.getDestinations();
        for(int i = 0; i < destinations.size(); i++){
            Destination destination = destinations.get(i);
            String name = destination.getName();
            if(!destinationSet.contains(name)){
                destinations.remove(i);
            }
        }
        Gson gson = new Gson();
        return gson.toJson(popularDestinationJson);

    }
}
