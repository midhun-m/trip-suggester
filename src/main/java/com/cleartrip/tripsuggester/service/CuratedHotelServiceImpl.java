package com.cleartrip.tripsuggester.service;

import com.cleartrip.tripsuggester.data.CuratedHotelData;
import com.cleartrip.tripsuggester.data.CuratedHotels;
import com.cleartrip.tripsuggester.util.TripSuggesterUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class CuratedHotelServiceImpl extends BaseTripSuggestService{

    @Autowired
    Environment env;

    private String getBlockMessage(){
        String message = env.getProperty("com.cleartrip.curated.hotels.block.message");
        return message;
    }


    private List<String> getCuratedHotelstoSuggest(){
        String curatedHotels = env.getProperty("com.cleartrip.curated.hotels.list");
        List<String> curatedHotelsList = Arrays.asList(curatedHotels.split(","));
        return curatedHotelsList;

    }

    private Map<String, Map<String, Object>> getHotelsData(){
        try{
            String curatedHotelsFile = env.getProperty("com.cleartrip.curated.hotels.file");
            String curatedHotelsJson = TripSuggesterUtil.readFromFile(curatedHotelsFile);
            Gson gson = new Gson();
            Type type = new TypeToken<Map<String, Map<String, Object>>>(){}.getType();
            Map<String, Map<String, Object>> thingsToDoData = gson.fromJson(curatedHotelsJson, type);
            return thingsToDoData;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;

    }

    public CuratedHotelData createCuratedHotelData(Map<String, Object> hotelData){
        CuratedHotelData curatedHotelData = new CuratedHotelData();
        if(hotelData.containsKey("title")){
            String title = (String) hotelData.get("title");
            curatedHotelData.setTitle(title);
        }
        if(hotelData.containsKey("image_urls")){
            List<String> imageUrls = (List<String>) hotelData.get("image_urls");
            curatedHotelData.setImageUrl(imageUrls);
        }
        if(hotelData.containsKey("page_url")){
            String pageUrl = (String) hotelData.get("page_url");
            curatedHotelData.setPageUrl(pageUrl);
        }
        return curatedHotelData;
    }


    @Override
    public String getTripDetailsJson() {

        CuratedHotels curatedHotels = new CuratedHotels();

        String message = getBlockMessage();
        curatedHotels.setMessage(message);

        List<String> hotelsToSuggest = getCuratedHotelstoSuggest();
        Map<String, Map<String, Object>> hotelsData = getHotelsData();

        if(hotelsToSuggest != null){
            List<CuratedHotelData> curatedHotelsData = new ArrayList<>();
            for(String hotel: hotelsToSuggest){
                if(hotelsData.containsKey(hotel)){
                    Map<String, Object> hotelData = hotelsData.get(hotel);
                    CuratedHotelData curatedHotelData = createCuratedHotelData(hotelData);
                    curatedHotelsData.add(curatedHotelData);
                }
            }
            curatedHotels.setHotelList(curatedHotelsData);
        }

        Gson gson = new Gson();
        String curatedHotelsJson = gson.toJson(curatedHotels);

        return curatedHotelsJson;
    }
}
