package com.cleartrip.tripsuggester.service;

import java.util.List;

public interface TripSuggestService {

    public String getSuggestedTripsFromCache(String key);

    public void addResultsToCache(String key, String value, int duration);

    public String getTripDetailsJson();

}
