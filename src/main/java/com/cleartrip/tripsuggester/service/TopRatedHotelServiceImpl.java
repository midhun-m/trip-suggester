package com.cleartrip.tripsuggester.service;

import com.cleartrip.tripsuggester.data.Hotel;
import com.cleartrip.tripsuggester.data.TopRatedHotels;
import com.cleartrip.tripsuggester.util.TripSuggesterUtil;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import com.google.gson.reflect.TypeToken;

@Component
public class TopRatedHotelServiceImpl extends  BaseTripSuggestService{

    @Autowired
    Environment env;


    private String getBlockMessage(){
        String message = env.getProperty("com.cleartrip.top.rated.block.message");
        return message;
    }

    public List<String> getHotelsToSuggest(){
        String hotelStr = env.getProperty("com.cleartrip.top.rated.hotels.list");
        try {
            List<String> hotelList = new ArrayList(Arrays.asList(hotelStr.split(",")));
            return hotelList;

        }catch(Exception e) {
            System.out.println("Error in property: com.cleartrip.top.rated.hotels.list" + e);
        }
        return null;
    }

    private List<Hotel> getHotelDataFromApi(List<String> hotelIds){
        Gson gson = new Gson();

        String url = env.getProperty("com.cleartrip.hotel.details.url");
        String hotelIdStr = gson.toJson(hotelIds);
        String response = TripSuggesterUtil.makePostRequest(url, hotelIdStr);
        List<Hotel> hotelDetailsList = new ArrayList<>();
        if(response != null && !response.equals("")){
            Type type = new TypeToken<List<Hotel>>(){}.getType();
            hotelDetailsList = gson.fromJson(response, type);
        }
        return hotelDetailsList;
    }

    @Override
    public String getTripDetailsJson() {
        TopRatedHotels topHotels = new TopRatedHotels();

        String message = getBlockMessage();
        topHotels.setMessage(message);

        List<String> hotelsToSuggest = getHotelsToSuggest();
        List<Hotel> hotelDataList = getHotelDataFromApi(hotelsToSuggest);
        topHotels.setHotels(hotelDataList);

        Gson gson = new Gson();
        String tripDetailsJson = gson.toJson(topHotels);
        return tripDetailsJson;
    }
}
