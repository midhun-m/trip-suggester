package com.cleartrip.tripsuggester.service;

import com.cleartrip.tripsuggester.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class BaseTripSuggestService implements TripSuggestService{

    @Autowired
    CacheManager redisCacheManager;

    @Override
    public String getSuggestedTripsFromCache(String key) {
        String suggestedTrips = redisCacheManager.getString(key);
        return suggestedTrips;
    }


    @Override
    public void addResultsToCache(String key, String result, int duration) {
        redisCacheManager.addString(key, result, duration);
    }
}
