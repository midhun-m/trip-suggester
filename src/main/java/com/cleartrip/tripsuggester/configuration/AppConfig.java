package com.cleartrip.tripsuggester.configuration;

import com.cleartrip.tripsuggester.cache.RedisCacheManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Configuration
public class AppConfig extends AsyncConfigurerSupport {

    @Value("${com.cleartrip.tripsuggester.redis.host}")
    String host;

    @Value("${com.cleartrip.tripsuggester.redis.port}")
    Integer port;

    @Bean
    public RedisCacheManager redisCacheManager(){
        System.out.println("Trying to connect to " + host + " : " + port);
        RedisCacheManager redisCacheManager;
        JedisPool jedisPool = new JedisPool(host, port);
        Jedis jedis = jedisPool.getResource();
        return new RedisCacheManager(jedisPool, jedis);
    }
}
